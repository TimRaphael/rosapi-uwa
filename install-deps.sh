#!/bin/bash

# Update apt cache
sudo apt-get update

# Wiring Pi
sudo apt-get install wiringpi

# Install libmpsse dependencies
sudo apt-get install swig libftdi-dev python-dev

# Download and compile libmpsse
echo "Compiling Libmpsse libraries..."
cd ~/
wget https://storage.googleapis.com/google-code-archive-downloads/v2/code.google.com/libmpsse/libmpsse-1.3.tar.gz
tar -zxf ./libmpsse-1.3.tar.gz
cd libmpsse-1.3/src
./configure
make
sudo make install
cd ~/

# Install X11 Libraries
echo "Installing X11 Libraries..."
sudo apt-get install libx11-dev

# Install Lirc-Client
echo "Installing Lirc-Client Dev Library..."
sudo apt-get install liblircclient-dev

# Install OpenCV C Headers
sudo apt-get install libopencv-highgui-dev libopencv-dev libopencv-imgproc-dev

# Install Eyebot Library and compile
git clone https://bitbucket.org/TimRaphael/libeyebot-uwa.git ~/eyebot
cd ~/eyebot/src/rob
make clean
make
sudo cp ~/eyebot/lib/libeyebot.a /usr/lib/

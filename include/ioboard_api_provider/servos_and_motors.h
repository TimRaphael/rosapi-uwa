/**
 * \file    servos_and_motors.h
 * \brief   Header file for the servos and motors functions
 * \author  Marcus Pham
 */

#ifndef SERVOS_AND_MOTORS_H_
#define SERVOS_AND_MOTORS_H_


 #ifdef __cplusplus
extern "C" {
#endif
#include "serial.h"
#include "hdt.h"
#include "types.h"
#ifdef __cplusplus
}
#endif



#define SERVO_LOW 0
#define SERVO_HIGH 255

int ENCODERBaseVal[4];
HDT_MOTOR *MOTORFirst;
HDT_SERVO *SERVOFirst;
int MOTORInitialised;
int SERVOInitialised;

int MOTORInit();
int SERVOInit();

int SERVORange(int servo, int low, int high);
int SERVOSet(int servo, int angle);
int SERVOSetRaw(int servo, int angle);
int MOTORDrive(int motor, int speed);
int MOTORDriveRaw(int motor, int speed);

int MOTORPID(int motor, int p, int i, int d); // Set motor [1..4] PID controller values [1..255]
int MOTORPIDoff(int motor);                   // Stop PID control loop
int MOTORSpeed(int motor, int ticks);         // Set controlled motor speed in ticks/100 sec

int ENCODERRead(int quad);                    // Read quadrature encoder [1..4]
int ENCODERReset(int quad);                   // Set encoder value to 0 [1..4]

#endif /* SERVOS_AND_MOTORS_H_ */

#include "ROSAPIProvider.h"

ROSAPIProvider::ROSAPIProvider()
{
	//called automatically from a subclass

	// init the publishers (to read)
	m_psdPub		  = m_nodeHandle.advertise<ioboard_api_provider::psd>(ROSAPITopics::PSD_TOPIC, 1000);
	m_digitalReadPub  = m_nodeHandle.advertise<ioboard_api_provider::digital>(ROSAPITopics::DIGITAL_READ_TOPIC, 1000);
	m_analogPub	      = m_nodeHandle.advertise<ioboard_api_provider::analog>(ROSAPITopics::ANALOG_TOPIC, 1000);
	//m_imuPub		  = m_nodeHandle.advertise<sensor_msgs::Imu>(IMU_TOPIC, 1000); 


	//m_imuSub		 = m_nodeHandle.subscribe(IMU_TOPIC, 1000, &ROSAPIProvider::imuCallback, this);

	// init the services
	//m_digitalSetup = m_nodeHandle.advertiseService("/ioboard/digital/config", &ROSAPIProvider::setupDigital, this);
}

void ROSAPIProvider::setupSubscribers()
{
	ROS_INFO("Setting up the subscribers in the parent class");

	// init the subscribers (to control)
	m_motorSub       	= m_nodeHandle.subscribe(ROSAPITopics::MOTOR_TOPIC, 1000, &ROSAPIProvider::motorCallback, this);
	m_servoSub	     	= m_nodeHandle.subscribe(ROSAPITopics::SERVO_TOPIC, 1000, &ROSAPIProvider::servoCallback, this);
	m_digitalWriteSub   = m_nodeHandle.subscribe(ROSAPITopics::DIGITAL_WRITE_TOPIC, 1000, &ROSAPIProvider::digitalWriteCallback, this);

	return;
}

void ROSAPIProvider::startRunLoop()
{
	this->setupSubscribers();

	ros::Rate loop_rate(m_loopRate);

	while(ros::ok())
	{
		// call the associated methods for the publishers.


		// Publish PSD State
		for (std::vector<int>::iterator it = m_psdIds.begin(); it != m_psdIds.end(); ++it)
		{
		 	ioboard_api_provider::psd psdMsg;
			psdMsg.data = this->getPSD(*it);
			psdMsg.id = *it;

			m_psdPub.publish(psdMsg);
		}

		// Publish Analog Pin state
		for(std::vector<int>::iterator it = m_analogPins.begin(); it != m_analogPins.end(); ++it)
		{
			ioboard_api_provider::analog analogMsg;
			analogMsg.data = this->getAnalog(*it);
			analogMsg.id = *it;

			m_analogPub.publish(analogMsg);
		}

		// Publish the state for the digital Read Pins
		for(std::vector<int>::iterator it = m_digitalReadPins.begin(); it != m_digitalReadPins.end(); ++it)
		{
			ioboard_api_provider::digital digitalMsg;
			digitalMsg.data = this->getDigital(*it);
			digitalMsg.id = *it;

			m_digitalReadPub.publish(digitalMsg);
		}

		// allow for subscriber interrupts
		ros::spinOnce(); 

		// sleep the loop
		loop_rate.sleep();
	}
}


void ROSAPIProvider::motorCallback(const ioboard_api_provider::motor& msg)
{
	ROS_INFO("Set Motor Called - ID: %d, Speed: %d", msg.id, msg.data);
	this->setMotor(msg.id, msg.data);
	return;
}

void ROSAPIProvider::servoCallback(const ioboard_api_provider::servo& msg)
{
	this->setServo(msg.id, msg.data);
	return;
}

void ROSAPIProvider::digitalWriteCallback(const ioboard_api_provider::digital& msg)
{
	this->setDigital(msg.id, msg.data);
	return;
}






#include "ROSAPIConsumer.h"

// Subclass our Consumer class so we can respond to async events.
class USALController: public ROSAPIConsumer {

	public:
	
		// These are the callback functions for our various sensors
		// These will be called when new data comes from any sensor
		// id = the number of the sensor and the data is the int value.
		// You will only recieve callbacks for the sensors that have been "setup"
		// in the Provider sub-class.
		// These must all be implemented for now, otherwise you'll get compiler errors
		// about missing abstract class methods (virtuals).
		virtual void recievedPsdData(int id, int data)
		{
			ROS_INFO("Recieved PSD Data with id: %d data: %d", id, data);
			return;
		}

		virtual void recievedAnalogData(int id, int data)
		{
			ROS_INFO("Recieved Analog Data with id: %d data: %d", id, data);
			return;
		}

		virtual void recievedDigitalData(int id, int value)
		{
			ROS_INFO("recievedDigitalData with id: %d value: %d", id, value);
			return;
		}

};

int main(int argc, char **argv)
{
	// As we're going to simply be calling methods on our controller, we need
	// to setup a ROS node for our control code.
	ros::init(argc, argv, "USALController");

	// Init a usalController instance from which we can issue commands.
	USALController* usalController = new USALController();

	// set our loop rate to 1Hz
	ros::Rate loop_rate(1);

	// Just setting up some inital test values.
	int motorSpeed = 0;
	int motorDiff = 10;
	int servoTicks = 0;
	int servoDiff = 50;
	int pinValue = 0;


	// Start our control / main run loop.
	while(ros::ok())
	{
		// needed to allow interrupts for callbacks from sensors
		ros::spinOnce();


		// Drive the motor speed, 100 --> 0 --> 100 changing every second (loop rate).
		if(motorSpeed == 100) { motorDiff = -10; }
		else {
			if(motorSpeed == 0) { motorDiff = 10; }
		}

		motorSpeed = motorSpeed + motorDiff;
		usalController->setMotor(1,motorSpeed);	// Set speed on motor #1

		// Drive the servo position, 0 --> 255 --> 0 changing every second (loop rate).
		if(servoTicks == 255) { servoDiff = -50; }
		else {
			if(servoTicks == 0) { servoDiff = 50; }
		}

		servoTicks = servoTicks + servoDiff;
		usalController->setServo(1, servoTicks); // Set the servo position on Servo #1

		// Write a 1 and a zero to the digital pin, alternating once a second.
		if(pinValue) { pinValue = 0; }
		else { pinValue = 1; }

		usalController->setDigital(4, pinValue); // Set the value on pin #1

		// to control the sleep phase of the loop rate.
		loop_rate.sleep();
	}

	// turn off the motor when ROS closes / when the node is killed.
	usalController->setMotor(1, 0);
}
#ifndef ROSAPITOPCICS_H
#define ROSAPITOPCICS_H

//  File Name : ROSAPITopics.h    Purpose : Global Constants for ROS Topics
namespace ROSAPITopics
{
	const std::string MOTOR_TOPIC     		= "/ioboard/motor";
	const std::string SERVO_TOPIC     		= "/ioboard/servo";
	const std::string PSD_TOPIC       		= "/ioboard/psd";
	const std::string DIGITAL_READ_TOPIC   	= "/ioboard/digital/read";
	const std::string DIGITAL_WRITE_TOPIC   = "/ioboard/digital/write";
	const std::string ANALOG_TOPIC    		= "/ioboard/analog";
	const std::string IMU_TOPIC       		= "/ioboard/imu";
}
#endif


#include "ros/ros.h"
#include "sensor_msgs/Imu.h"
#include "ioboard_api_provider/psd.h"
#include "ioboard_api_provider/analog.h"
#include "ioboard_api_provider/motor.h"
#include "ioboard_api_provider/servo.h"
#include "ioboard_api_provider/digital.h"
#include "ROSAPITopics.h"

class ROSAPIConsumer {


protected:
	ros::NodeHandle m_nodeHandle;

	// Setup subscribers
	ros::Subscriber m_psdSub;
	ros::Subscriber m_analogSub;
	ros::Subscriber m_digitalReadSub;

	// Setup the publishers for sending requests
	ros::Publisher m_motorPub;
	ros::Publisher m_servoPub;
	ros::Publisher m_digitalWritePub;


public:

	ROSAPIConsumer();


	void setMotor(int id, int percent);
	void setServo(int id, int ticks);
	void setDigital(int it, int value);

	void psdCallback(const ioboard_api_provider::psd& msg);
	void analogCallback(const ioboard_api_provider::analog& msg);
	void digitalCallback(const ioboard_api_provider::digital& msg);

	virtual void recievedPsdData(int id, int data) = 0;
	virtual void recievedAnalogData(int id, int data) = 0;
	virtual void recievedDigitalData(int id, int value) = 0;
};